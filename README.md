Forked from [Fil's gist](https://gist.github.com/Fil/cb7930254c9e7062114678d62d9be5ac) and made it minimalist:

* removed all but sphere
* filled `sphere` to give a visual clue when it is "interruped"


Fil's gist is about:

* Research for [d3-geo-projection/pull/86](https://github.com/d3/d3-geo-projection/pull/86) and [d3-geo/issues/46](https://github.com/d3/d3-geo/issues/46).
* Re-incorporating Jason Davies’ `clipPolygon()` code into d3v4.
* Code base at [Fil/d3-geo:clip-polygon](https://github.com/d3/d3-geo/pull/108).
* See also [https://bl.ocks.org/Fil/694ba0d0bc1fc4c24eb257dc210eb01a](https://bl.ocks.org/Fil/694ba0d0bc1fc4c24eb257dc210eb01a)

